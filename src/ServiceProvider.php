<?php namespace DPS\HtmlBuilder;

use Illuminate\View\Compilers\BladeCompiler;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->app[BladeCompiler::class]->directive('class', function ($list) {
            return "<?php echo collect({$list})->filter()->keys()->implode(' ') ?>";
        });

        $this->app[BladeCompiler::class]->directive('favicon', function () {
            try {
                $path = public_path('iconstats.json');

                if (file_exists($path)) {
                    $json = json_decode(file_get_contents($path), true);
                    return collect($json['html'])->implode("\n");
                }
            } catch (\Exception $e) {
                
            }
            
            return '';
        });
    }
}